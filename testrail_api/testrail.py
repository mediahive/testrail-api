"""api.py: api functions for testrail api wrapper"""

from ConfigParser import ConfigParser
import requests
import json

# Will error without setting the header
headers = {"Content-Type": "application/json"}


class TestRailAPI(object):

    def __init__(self):
        username = None
        password = None
        url = None

        config = ConfigParser()
        config.read('testrail.cfg')

        section = 'TESTRAIL'
        if config.has_section(section):
            if config.has_option(section, 'url'):
                url = config.get(section, 'url')
            if config.has_option(section, 'username'):
                username = config.get(section, 'username')
            if config.has_option(section, 'password'):
                password = config.get(section, 'password')

        self.url = url
        self.username = username
        self.password = password
        self.auth = (self.username, self.password)

        if self.url is None:
            raise ValueError('Need a url to connect to API!')
        if self.username is None:
            raise ValueError('Username must be declared!')
        if self.password is None:
            raise ValueError('Password must be set!')

    def pretty(payload):
        print json.dumps(payload, sort_keys=True,
                         indent=4, separators=(',', ':'))

    def get_case(self, id):
        """Returns an existing test case."""
        req = requests.get(self.url + 'get_case/' + str(id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_cases(self, project_id, suite_id, section_id=None):
        """Returns a list of test cases for a test suite."""
        id = project_id
        params = {'suite_id': suite_id}
        if section_id is not None:
            params = {'suite_id': suite_id, 'section_id': section_id}

        req = requests.get(self.url + 'get_cases/' + str(id),
                           params=params,
                           auth=self.auth, headers=headers)
        return req.json()

    def add_case(self, section_id, **kwargs):
        """Creates a new test case.

            testrail = TestRailAPI()
            testrail.add_case(section_id, title='Name of testcase')

        """

        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'add_case/' + str(section_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))
        return req.json()


    def update_case(self, id, **kwargs):
        """Updates an existing test case (partial updates are supported, i.e. you can submit and update specific
        fields only). This method supports the same POST fields as add_case."""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'update_case/' + str(id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req


    def delete_case(self, id):
        """Deletes and existing test case."""

        req = requests.post(self.url + 'delete_case/' + str(id),
                            auth=self.auth, headers=headers)
        return req.json()


    def get_case_fields(self):
        """Returns a list of available test case custom fields."""

        req = requests.get(self.url + 'get_case_fields/',
                           auth=self.auth, headers=headers)
        return req.json()


    def get_case_types(self):
        """Use the following API methods to request details about case type"""
        req = requests.get(self.url + 'get_case_types/',
                           auth=self.auth, headers=headers)

        return req.json()


    def get_milestone(self, milestone_id):
        """Returns an existing milestone."""
        req = requests.get(self.url + 'get_milestone/' + str(milestone_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_milestones(self, project_id):
        """Returns a list of milestones for a project."""
        req = requests.get(self.url + 'get_milestones/' + str(project_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def add_milestone(self, project_id, **kwargs):
        """Creates a new milestone."""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'add_milestone/' + str(project_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req.json()


    def update_milestone(self, milestone_id, **kwargs):
        """Updates an existing milestone (partial updates are supported, i.e. you can submit and update specific
        fields only)."""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'update_milestone/' + str(milestone_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req.json()


    def delete_milestone(self, milestone_id):
        """Deletes an existing milestone"""
        req = requests.post(self.url + 'delete_milestone/' + str(milestone_id),
                            auth=self.auth, headers=headers)

        return req.json()


    def get_project(self, project_id):
        """Returns an existing project."""
        req = requests.get(self.url + 'get_project/' + str(project_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_projects(self):
        """Returns a list of projects."""
        req = requests.get(self.url + 'get_projects/',
                           auth=self.auth, headers=headers)
        return req.json()

    def add_project(self, **kwargs):
        """Creates a new project (administrator status required)"""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'add_project/',
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req.json()

    def update_project(self, project_id, **kwargs):
        """Updates an existing project (partial updates are supported, i.e. you can submit and
        update specific fields only)."""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'update_project/' + str(project_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req.json()

    def delete_project(self, project_id):
        """Deletes an existing project."""

        req = requests.post(self.url + 'delete_project/' + str(project_id),
                            auth=self.auth, headers=headers)

        return req.json()

    def get_plan(self, plan_id):
        """Returns an existing test plan."""
        req = requests.get(self.url + 'get_plan/' + str(plan_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_plans(self, project_id):
        """Returns a list of test plans for a project."""
        req = requests.get(self.url + 'get_plans/' + str(project_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def add_plan(self, project_id, **kwargs):
        """Creates a new test plan"""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'add_plan/' + str(project_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req.json()


    def add_plan_entry(self, plan_id, **kwargs):
        """Creates a new test run for a test plan"""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'add_plan/' + str(plan_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req.json()


    def update_plan_entry(self, plan_id, entry_id, **kwargs):
        """Updates an existing test run in a plan (partial updates are supported, i.e. you can submit and update
        specific fields only)."""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'update_plan_entry/' + '%d/%d' % (plan_id, entry_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req.json()


    def close_plan(self, plan_id):
        """Closes an existing test plan and archives its test runs & results"""

        req = requests.post(self.url + 'close_plan/' + str(plan_id),
                            auth=self.auth, headers=headers)

        return req.json()


    def delete_plan(self, plan_id):
        """Deletes an existing test plan"""

        req = requests.post(self.url + 'delete_plan/' + str(plan_id),
                            auth=self.auth, headers=headers)

        return req.json()


    def delete_plan_entry(self, plan_id, entry_id):
        """Deletes an existing test run from a plan"""

        req = requests.post(self.url + 'delete_plan_entry/' + '%d/%d' % [plan_id, entry_id],
                            auth=self.auth, headers=headers)

        return req.json()


    def get_priorities(self):
        """Returns a list of available priorities."""

        req = requests.get(self.url + 'get_priorities/',
                           auth=self.auth, headers=headers)

        return req.json()


    def get_run(self, run_id):
        """Returns an existing test run."""
        req = requests.get(self.url + 'get_run/' + str(run_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_runs(self, project_id):
        """Returns a list of test runs for a project. Only returns those test runs that are not part of a test plan."""
        req = requests.get(self.url + 'get_runs/' + str(project_id),
                           auth=self.auth, headers=headers)
        return req.json()

    def add_run(self, project_id, **kwargs):
        """Creates a new test run."""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'add_run/' + str(project_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req

    def update_run(self, run_id, **kwargs):
        """Updates an existing test run (partial updates are supported, i.e. you can submit
        and update specific fields only)."""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'update_run/' + str(run_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req

    def close_run(self, run_id):
        """Closes an existing test run and archives its tests & results"""
        req = requests.post(self.url + 'close_run/' + str(run_id),
                            auth=self.auth, headers=headers)

        return req.json

    def delete_run(self, run_id):
        """Closes an existing test run and archives its tests & results"""
        req = requests.post(self.url + 'delete_run/' + str(run_id),
                            auth=self.auth, headers=headers)

        return req.json

    def get_section(self, section_id):
        """Returns an existing section."""
        req = requests.get(self.url + 'get_section/' + str(section_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_sections(self, project_id, suite_id):
        """Returns a list of sections for a project and test suite."""
        req = requests.get(self.url + 'get_sections/%d&suite_id=%d' % (project_id, suite_id,),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_test(self, test_id):
        """Returns an existing test."""
        req = requests.get(self.url + 'get_test/' + str(test_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_tests(self, run_id):
        """Returns a list of tests for a test run."""
        req = requests.get(self.url + 'get_tests/' + str(run_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_results(self, test_id):
        """Returns a list of test results for a test."""
        req = requests.get(self.url + 'get_results/' + str(test_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_results_for_case(self, run_id, case_id):
        """Returns a list of test results for a test run and case combination"""
        req = requests.get(self.url + 'get_results_for_case/%d/%d' % (run_id, case_id),
                           auth=self.auth, headers=headers)
        return req.json()

    def get_result_fields(self):
        """Returns a list of available test result custom fields"""
        req = requests.get(self.url + 'get_result_fields/',
                           auth=self.auth, headers=headers)

        return req.json()

    def add_result(self, test_id, **kwargs):
        """Creates a new test result."""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'add_result/' + str(test_id),
                            auth=self.auth, headers=headers,
                            data=json.dumps(fields))

        return req

    def add_result_for_case(self, run_id, case_id, **kwargs):
        """Creates a new test result for a test run and case combination"""
        fields = {}

        for key in kwargs:
            fields[key] = kwargs[key]

        req = requests.post(self.url + 'add_result_for_case/%d/%d' % (run_id, case_id),
                            auth=self.auth, headers=headers)

        return req

    def get_suite(self, suite_id):
        """Returns an existing test suite."""
        req = requests.get(self.url + 'get_suite/' + str(suite_id),
                           auth=self.auth, headers=headers)
        return req.json()


    def get_suites(self, project_id):
        """Returns a list of test suites for a project"""
        req = requests.get(self.url + 'get_suites/' + str(project_id),
                           auth=self.auth, headers=headers)

        return req.json()
