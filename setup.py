# -*- coding: utf-8 -*-
"""
tesrail-api
=======

A python wrapper for testrail api commands

Installation
------------

* pip install testrail-api

Usage (default)
---------------

1) import testrail api class

``from testrail_api.testrail import TestRailAPI``

2) Create an instance of the TestRailAPI class

``testrail = TestRailAPI()``

3) Interact with API via helper methods. For example:

``testrail.get_case(13398) => Argument correlates to individual test case ID``

``testrail.get_results(191929) => Argument correlates to individual test case results ID``

``testrail.add_case(3045, title='Name of testcase') => Argument correlates to test case section_id``
"""

from setuptools import setup

setup(
    name='testrail-api',
    packages=['testrail_api'],
    version='0.0.1',
    description='python wrapper for testrail api commands',
    url='https://erik_marty@bitbucket.org/mediahive/testrail-api.git',
    download_url='https://erik_marty@bitbucket.org/mediahive/testrail_api/tarball/0.0.1',
    author='Erik Marty',
    author_email='erikomarty@yahoo.com',
    license='Apache',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    install_requires=['requirements']
)
