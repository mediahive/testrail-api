testrail-api
============

Python wrapper for TestRail API v1


## Installation

```shell
pip install -r requirements.txt
```

### Config file

- Create a file named 'testrail.cfg'
- Make the content of file match the following:

```shell
[TESTRAIL]
url = https://mhqa.testrail.net/index.php?/api/v2/
username = <TESTRAIL_USERNAME>
password = <TESTRAIL_PASSWORD>
```

## Usage

1) import testrail api class

```shell
from testrail_api.testrail import TestRailAPI
```

2) Create an instance of the TestRailAPI class

```shell
testrail = TestRailAPI()
```

3) Interact with API via helper methods. For example:

	testrail.get_case(13398) => Argument correlates to individual test case ID
	
 	testrail.get_results(191929) => Argument correlates to individual test case results ID
 	
 	testrail.add_case(3045, title='Name of testcase') => Argument correlates to test case section_id
